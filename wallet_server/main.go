package main

import (
	"flag"
	"fmt"
	"log"
)

func init() {
	log.SetPrefix("Wallet Server:")

}

func main() {
	port := flag.Uint("port", 8080, "TCP Port Number for Wallet Server")
	getway := flag.String("gateway", "http://localhost:5000", "Blockchain Gateway")
	flag.Parse()

	fmt.Println("Wallet: ")
	ws := NewWalletServer(uint16(*port), *getway)
	ws.Run()

}
