package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/mswill/go-blockchain/block"
	"gitlab.com/mswill/go-blockchain/wallet"
	"io"
	"log"
	"net/http"
	"strconv"
)

var cache map[string]*block.Blockchain = make(map[string]*block.Blockchain)

type BlockchainServer struct {
	port uint16
}

func NewBlockchainServer(port uint16) *BlockchainServer {
	return &BlockchainServer{port}
}

func (bcs *BlockchainServer) Port() uint16 {
	return bcs.port
}

func (s BlockchainServer) GetBlockChain() *block.Blockchain {
	bc, ok := cache["blockchain"]
	if !ok {
		minerWallet := wallet.NewWallet()
		bc := block.NewBlockchain(minerWallet.BlockchainAddress(), s.Port())
		cache["blockchain"] = bc
		fmt.Printf("private_key: %v\n", minerWallet.PrivateKeyStr())
		fmt.Printf("public_key: %v\n", minerWallet.PublicKeyStr())
		fmt.Printf("blockchain_address: %v\n", minerWallet.BlockchainAddress())

	}
	return bc
}

func (s BlockchainServer) GetChain(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		w.Header().Add("Content-Type", "application/json")
		bc := s.GetBlockChain()
		m, _ := json.Marshal(bc)
		io.WriteString(w, string(m[:]))
	default:
		fmt.Printf("ERROR: Invalid http method ")
	}
}
func (s *BlockchainServer) Run() {
	http.HandleFunc("/", s.GetChain)
	log.Fatal(http.ListenAndServe("0.0.0.0:"+strconv.Itoa(int(s.Port())), nil))
}
